# Gemini auth

Sorry, I have no better name at the moment.

## What is this?

This is my first program written in Python.

This is piece of software for authentication and user management in geminispace. Authentication is done using TLS client certificates and is supported by few of Gemini browsers, for example [Lagrange](https://git.skyjake.fi/gemini/Lagrange). Gemini auth handles all the logic of creating account, linking and unlinking keys (impossible, but it have been done), and small SQLite database management – see <doc/authentication> for technical details. It also have nice user application connected to the [core library](lib/auth/__init__.py).

I assume, you use [stargazer](https://git.sr.ht/~zethra/stargazer/refs) as your Gemini server? Y'know, this small Rust code even I could compile on my old laptop (before I realized there are binary releases linked above).

## How do I run this?

Download this repository, create directory `data` because git doesn't keep empty directories, run `stargazer config.ini` – I store stargazer bin in the repo, that's why you see stargazer blacklisted in [.gitignore](.gitignore). Running stargazer from `$PATH` also should do.

Last step – open your favourite Gemini client and jump into gemini://localhost. That's all!

Have fun!

## Screenshots (comming soon)

## License

This program is licensed under GNU GPL license, version 3 or later (at your option). GNU GPL is [Free Software](https://gnu.org/philosophy/free-sw) license which gives you, the user, four essential freedoms:

1. Run
2. Study
3. Modify
4. Share

I believe everybody should have these four freedoms, and these should be among human rights, no matter which software you use. Unfortunately, most software developers want to take them away to control how you use your computer. GNU GPL license protects your freedom and freedom of others by putting one specific restriction – you have got freedom, now respect other people's freedom too! If you distribute GPL-covered program, modified or not, make sure it's distributed under the same terms.

    Gemini auth - authentication and user management in geminispace
    Copyright (C) 2022  Anedroid

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

[Copy of the GNU General Public License](LICENSE)

## How can I contribute?

What is critical here, is *security*. As you can see, this project is maintained by one person. It's very likely that I could miss something. If you want to contribute, security audits would be the most appreciated form of help.

Or maybe you have idea how to improve the documentation? Maybe you have some questions? Or just found a typo? You can report bugs and other issues as well as catch me on [Mastodon](https://mstdn.social/@anedroid). It's more fun to program if you know someone knows about your project.

I hope this piece of software - Gemini auth - will be somewhat useful for Gemini geeks.